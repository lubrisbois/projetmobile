package eu.epfc.projetexam.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import eu.epfc.projetexam.Activities.DetailedActivity;
import eu.epfc.projetexam.Adapters.ListPocketAdapter;
import eu.epfc.projetexam.Singleton.MoviesRepository;
import eu.epfc.projetexam.Network.MovieBase;
import eu.epfc.projetexam.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class PocketFragment extends Fragment implements ListPocketAdapter.ListItemClickListener {


    private ListPocketAdapter filmAdapter;
    public PocketFragment() {
        // Required empty public constructor
    }

    public void setMovies(){
        List<MovieBase> movieList =  MoviesRepository.getInstance().getPocketMovies(getContext());
        filmAdapter.setMovies(movieList);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();


        RecyclerView recyclerView = getView().findViewById(R.id.list_movies);

        filmAdapter = new ListPocketAdapter(this);
        setMovies();
        recyclerView.setAdapter(filmAdapter);

        Context context = getActivity();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public void onListItemClick(int clickedItemIndex) {
        Intent intent = new Intent(getActivity(), DetailedActivity.class);

        if(filmAdapter.getMovie(clickedItemIndex).getId() == -1){

            filmAdapter.setMovies(MoviesRepository.getInstance().nextPage());

        }
        else{
            intent.putExtra("movieId", "" + filmAdapter.getMovie(clickedItemIndex).getId());
            startActivity(intent);
        }

    }

}
