package eu.epfc.projetexam.Network;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "movies")
public class MovieBase {

    @PrimaryKey
    private int id;

    private String title;

    @SerializedName("vote_average")
    private double rating;

    @SerializedName("poster_path")
    private String frontImagePath;

    @Ignore
    MovieBase(){
    }

    public MovieBase(int id, String title, double rating, String frontImagePath){
        this.id = id;
        this.title = title;
        this.rating = rating;
        this.frontImagePath = frontImagePath;
    }

    public int getId(){ return id;}

    public String getTitle() {
        return title;
    }

    public double getRating() {
        return rating;
    }

    public String getFrontImagePath() {
        return frontImagePath;
    }
}
