package eu.epfc.projetexam.Singleton;

import android.content.Context;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import eu.epfc.projetexam.Model.MovieDAO;
import eu.epfc.projetexam.Model.MovieDatabase;
import eu.epfc.projetexam.Network.ApiResults;
import eu.epfc.projetexam.Network.JsonPlaceHolderApi;
import eu.epfc.projetexam.Network.MovieBase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MoviesRepository {
    public interface DataUIListener
    {
        void updateUI();
    }

    private static final MoviesRepository ourInstance = new MoviesRepository();

    private WeakReference<DataUIListener> listenerMain;
    private WeakReference<DataUIListener> listenerSearch;

    private List<MovieBase> movies = new ArrayList<>();
    private List<MovieBase> searchResults = new ArrayList<>();
    private JsonPlaceHolderApi jsonPlaceHolderApi;

    private final String APIKEY = "ea2dcee690e0af8bb04f37aa35b75075";
    private int page = 1;

    private MoviesRepository() {

    }

    public static MoviesRepository getInstance() {
        return ourInstance;
    }

    public List<MovieBase> getMovies(){
        return movies;
    }
    public List<MovieBase> getSearchResults() {
        return searchResults;
    }

    public List<MovieBase> getPocketMovies(Context context){
        MovieDAO movieDAO = MovieDatabase.getInstance(context).movieDAO();
        return movieDAO.getMovies();
    }

    public List<MovieBase> nextPage(){

        ++page;
        getPopularMovies();
        return movies;
    }

    private void retrofitInit(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

    }

    public void initListenerMain(DataUIListener listenerMain){
        this.listenerMain = new WeakReference<>(listenerMain);
    }
    public void initListenerSearch(DataUIListener listenerSearch){
        this.listenerSearch = new WeakReference<>(listenerSearch);
    }

    public void getPopularMovies(){
        retrofitInit();
        Call<ApiResults> call = jsonPlaceHolderApi.getPopularFilms("" + page, APIKEY);
        call.enqueue(new Callback<ApiResults>() {
            @Override
            public void onResponse(Call<ApiResults> call, Response<ApiResults> response) {
                if(!response.isSuccessful()){
                    return;
                }
                movies.addAll(movies.size(), response.body().getResults());

                // test if not nul
                listenerMain.get().updateUI();

            }

            @Override
            public void onFailure(Call<ApiResults> call, Throwable t) {
                Log.d("Adapter","request failed");

            }
        });

    }
    public void searchMovies(String query){
        retrofitInit();

        String urlEncodedQuery;
        try {
            urlEncodedQuery = URLEncoder.encode(query, "UTF-8");
        }catch(UnsupportedEncodingException ex){
            throw new RuntimeException(ex.getCause());
        }

        Call<ApiResults> call = jsonPlaceHolderApi.getSearchResults(APIKEY,urlEncodedQuery);
        call.enqueue(new Callback<ApiResults>() {
            @Override
            public void onResponse(Call<ApiResults> call, Response<ApiResults> response) {
                if(!response.isSuccessful()){
                    return;
                }
                searchResults.clear();
                searchResults.addAll(response.body().getResults());

                // test if not nul
                listenerSearch.get().updateUI();

            }

            @Override
            public void onFailure(Call<ApiResults> call, Throwable t) {
                Log.d("Adapter","request failed");

            }
        });

    }

    public boolean isInPocket(String movieId, Context context) {
        MovieDAO movieDAO = MovieDatabase.getInstance(context).movieDAO();
        MovieBase movie = movieDAO.getMovie(Integer.valueOf(movieId));
        if(movie != null) return true;
        else return false;
    }

    public void insertMovie(String movieId, Context context) {
        MovieDAO movieDAO = MovieDatabase.getInstance(context).movieDAO();
        MovieBase movie = getMovieBaseById(movieId);
        if(movie != null){
            movieDAO.movieInsert(movie);
        }
        else {
            Log.d("Error", "Id incorrect");
        }
    }

    public void deleteMovie(String movieId, Context context) {
        MovieDAO movieDAO = MovieDatabase.getInstance(context).movieDAO();
        MovieBase movie = getMovieBaseById(movieId);
        if(movie != null){
            movieDAO.movieDelete(movie);
        }
        else {
            Log.d("Error", "Id incorrect");
        }
    }

    public MovieBase getMovieBaseById(String movieId){

        for (MovieBase movie: movies) {
            if(movie.getId() == Integer.valueOf(movieId)){
                return movie;
            }
        }

        return null;
    }


}
