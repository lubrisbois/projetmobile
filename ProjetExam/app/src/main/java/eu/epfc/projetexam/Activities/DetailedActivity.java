package eu.epfc.projetexam.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import eu.epfc.projetexam.Singleton.MoviesRepository;
import eu.epfc.projetexam.Network.Genre;
import eu.epfc.projetexam.Network.ApiResultsTrailer;
import eu.epfc.projetexam.Network.JsonPlaceHolderApi;
import eu.epfc.projetexam.Network.MovieDetailed;
import eu.epfc.projetexam.Network.Video;
import eu.epfc.projetexam.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailedActivity extends AppCompatActivity {

    private JsonPlaceHolderApi jsonPlaceHolderApi;

    private TextView textViewTitle;
    private TextView textViewDateGenres;
    private TextView textViewRating;
    private TextView textViewDescription;
    private ImageView imageViewBack;
    private CheckBox checkBoxPocket;
    private Button buttonTrailer;
    private ProgressBar progressBar;

    private String trailerLink;
    private MovieDetailed movie;
    private String movieId;
    private final String APIKEY = "ea2dcee690e0af8bb04f37aa35b75075";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceHolderApi= retrofit.create(JsonPlaceHolderApi.class);

        getViews();

        Intent intent = getIntent();
        movieId = (String) intent.getSerializableExtra("movieId");

        databaseCheck(movieId);
        setDataRetrofit(movieId);
    }

    private void databaseCheck(String movieId) {
        if(MoviesRepository.getInstance().isInPocket(movieId,this)){
            checkBoxPocket.setChecked(true);
        }
    }

    private void getViews(){
        textViewTitle = findViewById(R.id.textView_detailed_title);
        textViewDateGenres = findViewById(R.id.textView_detailed_dateGenres);
        textViewRating = findViewById(R.id.textView_detailed_rating);
        textViewDescription = findViewById(R.id.textView_detailed_description);
        imageViewBack = findViewById(R.id.imageView_detailed_back);
        checkBoxPocket = findViewById(R.id.checkBoxPocket);
        buttonTrailer = findViewById(R.id.button);
        buttonTrailer.setActivated(false);
        progressBar = findViewById(R.id.progress_bar);
    }

    //Intitialise les données des composants graphiques via retrofit
    private void setDataRetrofit(String movieId){
        Call<MovieDetailed> call = jsonPlaceHolderApi.getMovieDetails(movieId, APIKEY);

        call.enqueue(new Callback<MovieDetailed>() {
            @Override
            public void onResponse(Call<MovieDetailed> call, Response<MovieDetailed> response) {
                if(!response.isSuccessful()){
                    dialogEndActivity();
                    return;
                }
                movie = response.body();
                retrofitSetViews();
            }

            @Override
            public void onFailure(Call<MovieDetailed> call, Throwable t) {
                dialogEndActivity();
            }
        });

        Call<ApiResultsTrailer> call2 = jsonPlaceHolderApi.getTrailers(movieId, APIKEY);

        call2.enqueue(new Callback<ApiResultsTrailer>() {
            @Override
            public void onResponse(Call<ApiResultsTrailer> call, Response<ApiResultsTrailer> response) {
                if(!response.isSuccessful()){
                    return;
                }
                List<Video> videos = new ArrayList<>(response.body().getResults());
                if (videos != null && videos.size() != 0) {
                    trailerLink = videos.get(0).getKey();
                    buttonTrailer.setActivated(true);
                }
            }

            @Override
            public void onFailure(Call<ApiResultsTrailer> call, Throwable t) {

            }
        });
    }
    // Set views data after the retrofit call
    private void retrofitSetViews(){

        if(movie != null) {
            String genres = "";
            for(Genre genre : movie.getGenres()){
                genres += genre.getName() + " ";
            }

            textViewTitle.setText(movie.getTitle());
            textViewDateGenres.setText(movie.getReleaseDate() + " - " + genres);
            textViewRating.setText("Rating :" + movie.getRating());
            textViewDescription.setText(movie.getOverview());
            Picasso.get().load("https://image.tmdb.org/t/p/w500" + movie.getBackImagePath()).into(imageViewBack);

            progressBar.setVisibility(View.INVISIBLE);
            textViewTitle.setVisibility(View.VISIBLE);
            textViewDateGenres.setVisibility(View.VISIBLE);
            textViewRating.setVisibility(View.VISIBLE);
            textViewDescription.setVisibility(View.VISIBLE);
            imageViewBack.setVisibility(View.VISIBLE);

        }
    }

    // Show an error dialog + finish the activity
    private void dialogEndActivity(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setMessage("Les détails du film n'ont pas pu être chargés.\nVeuillez vérifier vos paramètres réseaux");

        dialogBuilder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();

        Log.d("Error", "Connection Failed");
    }

    public void onClickVideoButton(View view) {
        if(trailerLink != null){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + trailerLink));
            startActivity(intent);
        }
        else {
            Log.d("Error", "Trailer link empty");
        }
    }

    public void onClickPocketCheckbox(View view){
        if(checkBoxPocket.isChecked()){
            MoviesRepository.getInstance().insertMovie(movieId,this);
        }
        else {
            MoviesRepository.getInstance().deleteMovie(movieId,this);
        }
    }

    public void onSearch() {
        try{
            String test = URLEncoder.encode("Salut ceci est un test","UTF-8");
            Log.d("Error", "dab \n"+ test);
        }catch(UnsupportedEncodingException error){

        }
    }
}
