package eu.epfc.projetexam.Adapters;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import eu.epfc.projetexam.Network.MovieBase;
import eu.epfc.projetexam.R;
import eu.epfc.projetexam.Singleton.MoviesRepository;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.FilmViewHolder> {

    private List<MovieBase> searchResults = MoviesRepository.getInstance().getSearchResults() ;
    public MovieBase getMovie(int index){
        return searchResults.get(index);
    }
    final private ListItemClickListener listItemClickListener;

    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex);
    }

    public SearchAdapter(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    @Override
    public SearchAdapter.FilmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());

        View filmView =
                layoutInflater.inflate(R.layout.film_item_layout,parent,false);


        return new FilmViewHolder(filmView);
    }



    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.FilmViewHolder filmViewHolder, int i) {
        ViewGroup itemViewGroup = (ViewGroup) filmViewHolder.itemView;
        TextView titleTextView = itemViewGroup.findViewById(R.id.text_item_title);
        ImageView imageView = itemViewGroup.findViewById(R.id.image);
        TextView abstractTextView = itemViewGroup.findViewById(R.id.text_item_rating);

        MovieBase movie = searchResults.get(i);

        Picasso.get().load("https://image.tmdb.org/t/p/w500" + movie.getFrontImagePath()).into(imageView);
        titleTextView.setText(movie.getTitle());
        abstractTextView.setText("Rating : " + movie.getRating());

    }

    @Override
    public int getItemCount() {
        if (searchResults != null && searchResults.size() != 0) {
            return searchResults.size();
        } else {
            return 0;
        }
    }
    public void setMovies(List<MovieBase> movies) {
        this.searchResults = movies;
        this.notifyDataSetChanged();
    }

    public class FilmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        public FilmViewHolder(@NonNull View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            int clickedPosition = this.getAdapterPosition();
            SearchAdapter.this.listItemClickListener.
                    onListItemClick(clickedPosition);
        }
    }
}

