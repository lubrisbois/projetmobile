package eu.epfc.projetexam.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import eu.epfc.projetexam.Fragments.ListFragment;
import eu.epfc.projetexam.Fragments.PocketFragment;
import eu.epfc.projetexam.R;

public class CustomPagerAdapter extends FragmentPagerAdapter {

    private Context context;

    public CustomPagerAdapter(FragmentManager fm, Context c) {
        super(fm);
        context = c;
    }

    ListFragment listFragment = new ListFragment();

    PocketFragment pocketFragment = new PocketFragment();

    public ListFragment getListFragment(){
        return  listFragment;
    }
    public PocketFragment getListPocketFragment(){
        return  pocketFragment;
    }

    public CustomPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public String getPageTitle(int i){
        if(i == 0)
            return context.getResources().getString(R.string.title_list_movies) ;
        else
            return context.getResources().getString(R.string.pocket_movies);
    }

    @Override
    public Fragment getItem(int i) {

        if(i == 0)
            return listFragment;
        else
            return pocketFragment;



    }




}
