package eu.epfc.projetexam.Model;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import eu.epfc.projetexam.Network.MovieBase;

@Database(entities = {MovieBase.class}, version = 1, exportSchema = false)
public abstract class MovieDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "NYTimesReaderDB";
    private static MovieDatabase sInstance;

    public static MovieDatabase getInstance(Context context) {
        if (sInstance == null) {
        RoomDatabase.Builder<MovieDatabase> dbBuilder =
                Room.databaseBuilder(context.getApplicationContext(), MovieDatabase.class, MovieDatabase.DATABASE_NAME);
        /**********************************/
        dbBuilder.allowMainThreadQueries();
        /**********************************/
        sInstance = dbBuilder.build();
        }
        return sInstance;
    }
    public abstract MovieDAO movieDAO();
}
