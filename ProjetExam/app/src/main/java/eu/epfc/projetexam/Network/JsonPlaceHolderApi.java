package eu.epfc.projetexam.Network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JsonPlaceHolderApi {

    @GET("3/movie/popular")
    Call<ApiResults> getPopularFilms(@Query("page")String page, @Query("api_key")String apiKey);

    @GET("3/movie/{id}")
    Call<MovieDetailed> getMovieDetails(@Path("id")String id, @Query("api_key")String apiKey);

    @GET("3/movie/{id}/videos")
    Call<ApiResultsTrailer> getTrailers(@Path("id") String id, @Query("api_key")String apiKey);

    // https://api.themoviedb.org/3/search/movie?api_key={api_key}&query=Jack+Reacher
    @GET("3/search/movie")
    Call<ApiResults> getSearchResults(@Query("api_key")String apiKey, @Query("query")String searchQuery);
}
