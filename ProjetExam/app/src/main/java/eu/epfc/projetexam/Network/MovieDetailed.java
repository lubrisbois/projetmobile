package eu.epfc.projetexam.Network;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieDetailed {

    private String title;

    private String overview;

    private List<Genre> genres;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("vote_average")
    private double rating;

    @SerializedName("backdrop_path")
    private String backImagePath;

    @SerializedName("poster_path")
    private String frontImagePath;

    private boolean inPocket;

    /*****************************************************************************************/

    public String getTitle() {
        return title;
    }

    public String getOverview() {
        return overview;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public double getRating() {
        return rating;
    }

    public String getBackImagePath() {
        return backImagePath;
    }

    public boolean isInPocket() {
        return inPocket;
    }

    public String getFrontImagePath() {
        return frontImagePath;
    }
}
