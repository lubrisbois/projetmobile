package eu.epfc.projetexam.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import eu.epfc.projetexam.Singleton.MoviesRepository;
import eu.epfc.projetexam.Network.MovieBase;
import eu.epfc.projetexam.R;


public class ListFilmAdapter extends RecyclerView.Adapter<ListFilmAdapter.FilmViewHolder> {

    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex);
    }


    private List<MovieBase> movies = MoviesRepository.getInstance().getMovies() ;

    final private ListItemClickListener listItemClickListener;

    public MovieBase getMovie(int index){
        return movies.get(index);
    }

    public ListFilmAdapter(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    @Override
    public ListFilmAdapter.FilmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());

        View filmView =
                layoutInflater.inflate(R.layout.film_item_layout,parent,false);


        return new FilmViewHolder(filmView);
    }



    @Override
    public void onBindViewHolder(@NonNull ListFilmAdapter.FilmViewHolder filmViewHolder, int i) {
        ViewGroup itemViewGroup = (ViewGroup) filmViewHolder.itemView;
        TextView titleTextView = itemViewGroup.findViewById(R.id.text_item_title);
        ImageView imageView = itemViewGroup.findViewById(R.id.image);
        TextView abstractTextView = itemViewGroup.findViewById(R.id.text_item_rating);

        if(i == movies.size()) {
            titleTextView.setText("\n            Next Page");
            Picasso.get().load("https://image.tmdb.org/t/p/w500" + null).into(imageView);
            abstractTextView.setText("");
        }
        else {
            MovieBase movie = movies.get(i);


            Picasso.get().load("https://image.tmdb.org/t/p/w500" + movie.getFrontImagePath()).into(imageView);

            titleTextView.setText(movie.getTitle());

            abstractTextView.setText("Rating : " + movie.getRating());
        }
    }

    @Override
    public int getItemCount() {
        if (movies != null && movies.size() != 0) {
            return movies.size()+1;
        } else {
            return 0;
        }
    }

    public void setMovies(List<MovieBase> movies) {
        this.movies = movies;
        this.notifyDataSetChanged();
    }

    public class FilmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        public FilmViewHolder(@NonNull View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            int clickedPosition = this.getAdapterPosition();
                ListFilmAdapter.this.listItemClickListener.
                    onListItemClick(clickedPosition);
        }
    }
}
