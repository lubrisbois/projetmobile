package eu.epfc.projetexam.Network;

public class Video {

    private String id;
    private String key;
    private String type;
    private String name;

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
