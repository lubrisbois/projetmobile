package eu.epfc.projetexam.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import eu.epfc.projetexam.Network.MovieBase;
import eu.epfc.projetexam.R;

public class ListPocketAdapter extends RecyclerView.Adapter<ListPocketAdapter.FilmViewHolder> {
    public interface ListItemClickListener{
        void onListItemClick(int clickedItemIndex);
    }


    private List<MovieBase> movies;

    final private ListPocketAdapter.ListItemClickListener listItemClickListener;

    public MovieBase getMovie(int index){
        return movies.get(index);
    }

    public ListPocketAdapter(ListPocketAdapter.ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    @Override
    public FilmViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater =
                LayoutInflater.from(parent.getContext());

        View filmView =
                layoutInflater.inflate(R.layout.film_item_layout,parent,false);




        return new ListPocketAdapter.FilmViewHolder(filmView);
    }


    @Override
    public void onBindViewHolder(@NonNull FilmViewHolder filmViewHolder, int i) {
        MovieBase movie = movies.get(i);

        ViewGroup itemViewGroup = (ViewGroup)filmViewHolder.itemView;

        ImageView imageView = itemViewGroup.findViewById(R.id.image);
        Picasso.get().load("https://image.tmdb.org/t/p/w500" + movie.getFrontImagePath()).into(imageView);

        TextView titleTextView = itemViewGroup.findViewById(R.id.text_item_title);
        titleTextView.setText(movie.getTitle());

        TextView abstractTextView = itemViewGroup.findViewById(R.id.text_item_rating);
        if(movie.getTitle() != "Next Page")
            abstractTextView.setText("Rating : "+movie.getRating());
        else
            abstractTextView.setText("");
    }

    @Override
    public int getItemCount() {
        if (movies != null) {
            return movies.size();
        } else {
            return 0;
        }
    }

    public void setMovies(List<MovieBase> movies) {
        this.movies = movies;
        this.notifyDataSetChanged();
    }

    public class FilmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        public FilmViewHolder(@NonNull View itemView) {

            super(itemView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {

            int clickedPosition = this.getAdapterPosition();
            ListPocketAdapter.this.listItemClickListener.
                    onListItemClick(clickedPosition);
        }
    }


}
