package eu.epfc.projetexam.Model;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import eu.epfc.projetexam.Network.MovieBase;

@Dao
public interface MovieDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void movieInsert(MovieBase movie);

    @Query("SELECT * FROM movies WHERE id == :movieId")
    MovieBase getMovie(int movieId);

    @Query("SELECT * FROM movies")
    List<MovieBase> getMovies();

    @Delete
    void movieDelete(MovieBase movie);
}
