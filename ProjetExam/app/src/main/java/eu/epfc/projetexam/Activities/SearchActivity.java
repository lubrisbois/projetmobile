package eu.epfc.projetexam.Activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

import eu.epfc.projetexam.Adapters.SearchAdapter;
import eu.epfc.projetexam.Network.MovieBase;
import eu.epfc.projetexam.R;
import eu.epfc.projetexam.Singleton.MoviesRepository;

public class SearchActivity extends AppCompatActivity implements MoviesRepository.DataUIListener, SearchAdapter.ListItemClickListener{

    private SearchAdapter searchAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {

            MoviesRepository.getInstance().initListenerSearch(this);

            String query = intent.getStringExtra(SearchManager.QUERY);
            setTitle(query);

            MoviesRepository.getInstance().searchMovies(query);

            RecyclerView searchResultsRecyclerView = findViewById(R.id.search_results);
            // set the adapter of the RecyclerView
            searchAdapter = new SearchAdapter(this);
            searchResultsRecyclerView.setAdapter(searchAdapter);

            // set the layoutManager of the recyclerView
            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            searchResultsRecyclerView.setLayoutManager(layoutManager);

        }
    }
    @Override
    public void onListItemClick(int clickedItemIndex) {
        Intent intent = new Intent(this, DetailedActivity.class);

        intent.putExtra("movieId", "" + searchAdapter.getMovie(clickedItemIndex).getId());
        startActivity(intent);
    }

    @Override
    public void updateUI() {
        List<MovieBase> searchResults =  MoviesRepository.getInstance().getSearchResults();
        searchAdapter.setMovies(searchResults);
    }
}
