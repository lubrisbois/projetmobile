package eu.epfc.projetexam.Activities;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;


import eu.epfc.projetexam.Adapters.CustomPagerAdapter;
import eu.epfc.projetexam.Singleton.MoviesRepository;
import eu.epfc.projetexam.R;

public class MainActivity extends AppCompatActivity implements MoviesRepository.DataUIListener {


    CustomPagerAdapter customViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MoviesRepository.getInstance().initListenerMain(this);

        TabLayout tabLayout = findViewById(R.id.tab_layout);

        ViewPager viewPager =  findViewById(R.id.pager);
        customViewPager = new CustomPagerAdapter(getSupportFragmentManager(),this);
        viewPager.setAdapter(customViewPager);
        tabLayout.setupWithViewPager(viewPager);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);


    }

    @Override
    public void updateUI() {

        customViewPager.getListFragment().setMovies();
        customViewPager.getListPocketFragment().setMovies();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        SearchView search = (SearchView) menu.findItem(R.id.actionbar_searchbutton).getActionView();
        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(this, SearchActivity.class)));
        search.setQueryHint(getResources().getString(R.string.search_hint));

        return true;
    }

}
